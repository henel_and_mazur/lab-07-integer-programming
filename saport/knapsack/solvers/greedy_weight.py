from saport.knapsack.solvers.greedy import AbstractGreedySolver
from saport.knapsack.model import Item


class GreedySolverWeight(AbstractGreedySolver):
    """
    A greedy solver for the knapsack problems. 
    Uses weight as the greedy heuristic. 
    """
    def greedy_heuristic(self, item: Item) -> float:
        return -item.weight