from copy import deepcopy
from saport.integer.abstract_solver import AbstractIntegerSolver
from saport.simplex import solver as lpsolver
from saport.integer.model import Model
from saport.integer.solution import Solution
from saport.simplex.expressions import constraint as ssecon
from saport.simplex.expressions import expression as sseexp
from saport.simplex import solution as lpsolution
import math

class LinearRelaxationSolver(AbstractIntegerSolver):
    """
        Naive branch and bound solver for general integer programming problems
        using a linear relaxation approach. 
    """  

    def _solving_routine(self):
        self._branch_and_bound(self.model)
        self.best_solution = Solution.with_linear_solution(self.model, self.best_solution, not self.interrupted)
           
    def _branch_and_bound(self, model: Model):
        simplexSolver = lpsolver.Solver()
        simpelexSolution = simplexSolver.solve(model)
        lowerBound = -1000000
        upperBoud = None
        solutionsArray = []
        firstRow = []
        if simpelexSolution.is_feasible:
            firstRow.append({simpelexSolution: self.model})
            solutionsArray.append(firstRow)
        #TODO: implement a branch and bound procedure analogically to the one in the knaspack solver
        #      but use the simplex to relax the problem
        #
        # tip 1: use self.find_float_assignment to select the branching variable
        # tip 2: use self._model_with_new_constraint to extend model with a new constraint
        # tip 3: remember to check for the timeout and set the self.interrupted to True when it happens
        # tip 4: remember to handle infeasible and unbounded models, is_unbounded / is_feasible / has_assignemnt methods may be handy
        depthIndex = 0
        while True:
            solutionsArray.append([])
            for rowIndex in range(len(solutionsArray[depthIndex])):
                parentModel = list(solutionsArray[depthIndex][rowIndex].values())[0]
                parentSolution = list(solutionsArray[depthIndex][rowIndex].keys())[0]
                upperBoud = parentSolution.objective_value()
                if upperBoud <= lowerBound:
                    continue
                floatVariable = self._find_float_assignment(parentSolution)
                if floatVariable is not None: #solution has float variable
                    mini = 0
                    maxi = 0
                    while parentSolution.value(floatVariable) > (mini + 1):
                        mini += 1
                    while parentSolution.value(floatVariable) > maxi:
                        maxi += 1
                    leftSolver = lpsolver.Solver()
                    new_left_model = self._model_with_new_constraint(parentModel, floatVariable >= maxi)
                    leftSolution = leftSolver.solve(new_left_model)
                    if leftSolution.is_feasible:
                        solutionsArray[depthIndex+1].append({leftSolution: new_left_model})
                    rightSolver = lpsolver.Solver()
                    new_right_model = self._model_with_new_constraint(parentModel, floatVariable <= mini)
                    rightSolution = rightSolver.solve(new_right_model)
                    if rightSolution.is_feasible:
                        solutionsArray[depthIndex+1].append({rightSolution: new_right_model})
                else: # solution doesn't have float variable
                    temp = list(solutionsArray[depthIndex][rowIndex].keys())[0]
                    if temp.objective_value() > lowerBound:
                        self.best_solution = temp
                        lowerBound = temp.objective_value()
            depthIndex += 1
            if len(solutionsArray[depthIndex]) == 0:
                break
        pass 

        
    def _find_float_assignment(self, solution: lpsolution.Solution) -> sseexp.Variable:
        #TODO: find an variable that has non-integer value in the solution
        # tip: due to numeric errors some variables can have "almost integer" value, like 0.9999999 or 1.0000001
        #      make sure they are still counted as integers
        is_integer = False
        i = 0
        if solution.is_feasible and solution.is_bounded:
            for number in solution.assignment(self.model):
                is_integer = False
                mini = 0
                maxi = 0
                while number > (mini + 1):
                    mini += 1
                while number > maxi:
                    maxi += 1
                if maxi - number < 0.00001:
                    is_integer = True
                if number - mini < 0.00001:
                    is_integer = True
                if is_integer is False:
                    return self.model.variables[i]

                i += 1
        return None

    def _model_with_new_constraint(self, model: Model, constraint: ssecon.Constraint) -> Model:
        new_model = deepcopy(model)
        new_model.add_constraint(constraint)
        return new_model
