from copy import deepcopy
from typing import Set, Dict, List

from saport.integer.abstract_solver import AbstractIntegerSolver
from saport.integer.model import BooleanModel
from saport.integer.solution import Solution
from saport.simplex.expressions import expression as sseexp

class UnsupportedModel(Exception):
    pass


class ImplicitEnumerationSolver(AbstractIntegerSolver):
    """
        Naive branch and bound solver for boolean integer programming problems
        using an implicit enumeration approach.

        Attributes:
        ----------
        _flipped_variables: Set[Variable]
            variables that had to be "flipped" in order to get an enumerable model
    """
    _flipped_variables: Set[sseexp.Variable]

    def __init__(self):
        self._flipped_variables = set()

    def _solving_routine(self):
        if not isinstance(self.model, BooleanModel):
            raise UnsupportedModel(
                f"the model of type {type(self.model)} is not supported by the implicit enumeration solver")
        self.model.simplify()
        self._enum_model = self._preprocess_model(deepcopy(self.model))
        self._branch_and_bound(dict(), set(self._enum_model.variables))

        if self.best_solution is None:
            self.best_solution = Solution.infeasible(self.model)
        else:
            self.best_solution = self._unflipped_solution(self.best_solution)

    def _preprocess_model(self, model: BooleanModel) -> BooleanModel:
        # TODO:
        # 1. change objective to max
        if model.objective.type.value == -1:
            model.objective.invert()
        model.constraints[0].type = model.constraints[0].type.LE
        # 2. change all constraints to LE:
        #   - first, EQ constraints are translated to two constraints LE and GE
        for i in range(len(model.constraints)):
            if model.constraints[i].type.value == 0:
                model.add_constraint(model.constraints[i].expression >= model.constraints[i].bound)
                model.constraints[i] = (model.constraints[i].expression <= model.constraints[i].bound)
        #   - second, GE constraints are translated to LE
        for i in range(len(model.constraints)):
            if model.constraints[i].type.value == 1:
                model.constraints[i].invert()
        # 3. "flip" all variables that have positive coefficients in the objective:
        #   - first, change their coefficient to negative in the objective
        for i in range(len(model.objective.expression.coefficients(model))):
            if model.objective.expression.coefficients(model)[i] > 0:
        #   - store all the flipped variables in self._flipped_variables
                self._flipped_variables.add(model.variables[i])
                model.objective.expression.set_coefficient(model.variables[i],
                                                           model.objective.expression.coefficients(model)[i] * -1)
        #   - second, go through every constraint and replace flipped variable 'x' with (1-x)
        for i in range(len(model.constraints)):
            for flipped in self._flipped_variables:
                value = model.constraints[i].expression.get_coefficient(flipped)
        #     the free constants have to be subtracted from the right side (constraint.bound)
                model.constraints[i].bound -= value
                model.constraints[i].expression.set_coefficient(flipped, -1 * value)
        #   tip. use set_coefficient and get coefficient methods in the Expression class
        #         or iterate directly over the the experession.atoms list
        return model

    def _branch_and_bound(self, partial_assignment: Dict[sseexp.Variable, int], left: Set[sseexp.Variable]):
        # TODO:
        # 1) find the upper bound which is the best possible objective value for the given partial assignment
        #    (all the free variables - they're stored in the 'left' argument - have to be set to zero)
        # 2) if the upper bound is lower than the current lower bound, then the branch may be pruned
        #    as in every other branch and bound algorithm 
        #    tip. use self._lower_bound()
        # 3) then check if the partial assignment is still satisfiable
        #    if it's not, then the branch should be pruned
        #    tip. use self._is_model_satisfiable_with_partial_assignment.
        # 4) then check if it's feasible solution
        #    if yes, then prune the branch and update the self.best_solution as necessary
        #    tip 1. use self._total_infeasibility
        #    tip 2. use Solution.with_assignment
        # 5) otherwise, choose the branching variable and branch on it
        #    tip 1. use self._select_var_to_branch
        #    tip 2. remember to branch with updated "left" and "partial_assignment"
        #
        # tip 1. note that self._enum_model contains the model processed in the algorithm's first step
        # tip 2. remember to check for timeout (self.timeout()) and set self.interrupted to True

        pass

    def _is_model_satisfiable_with_partial_assignment(self, partial_assignment: Dict[sseexp.Variable, int]) -> bool:
        # TODO:
        # Check for each constraint whether the partial assignments can be extended to satisfy the constraint
        # 1) create an "optimistic" assignment based on the partial_assignment and the tested constraint
        #   - variables fixed in the assignment should have value as fixed
        #   - other ones should be 1 if they have negative coefficient in the constraint, otherwise 0
        # 2) evaluate the constraint expression and check if it satisfies the bound
        # 3) if at least one constraint can't be satisfied, the model is not satisfiable

        return True

    def _select_var_to_branch(self, left: Set[sseexp.Variable], partial_assignment: Dict[sseexp.Variable,
                                                                                         int]) -> sseexp.Variable:
        # TODO: for every variable in the problem, check what would be the total infeasibility, 
        # if the variable was assigned 1 and all the remaining variables were set to 0
        # Return variable with the least infeasibility

        minVar = self.model.variables[0]
        assignment = []
        infeas = []
        for var in partial_assignment.keys():
            temp = partial_assignment[var]
            partial_assignment[var] = 1
            for value in partial_assignment.values():
                assignment.append(value)
            infeas.append(self._total_infeasibility(assignment))
            if min(infeas) <= self._total_infeasibility(assignment):
                minVar = var
            assignment.clear()
            partial_assignment[var] = temp

        return minVar

    def _total_infeasibility(self, assignment: List[int]) -> int:
        total_infeasibility = 0
        # TODO: check how much the constraints are violated.
        # For each constraint evaluate the constraint expression and check how much its value surpasses the bound.
        # Then add the result to the total_infeasibility
        for constraint in BooleanModel.constraints:                    # wszystkie constrainty sa LE (patrz wyzej, _preprocess_modelL: 2. change all constraints to LE)
            wartosc = constraint.expression.evaluate(assignment)
            ograniczenie = constraint.bound
            if wartosc > ograniczenie:
                total_infeasibility += (wartosc - ograniczenie)

        return total_infeasibility

    def _unflipped_solution(self, solution: Solution) -> Solution:
        # TODO:
        # Return the "unflipped" solution.
        # - "unflip" means that values assigned to the variables in self._flipped_variables should get flipped again, i. e.
        #   if the flipped variable got value 0 in the solution, it should be set to 1 (and vice versa)
        for var in self._flipped_variables:
            if solution.assignment[var.index] == 0:
                solution.assignment[var.index] = 1
            else:
                solution.assignment[var.index] = 0

        return solution
